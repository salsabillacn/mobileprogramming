import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { TouchableOpacity , View, Image, StyleSheet, Text, TextInput } from 'react-native';

export default function Signup() {
    const [text, onChangeText] = React.useState(null);
    const [number, onChangeNumber] = React.useState(null);
    return (
        <View style={styles.container}>
          <Image
            style={styles.stretch}
            source={require('./assets/filetype-08-512.png')}
          />
          <StatusBar style="auto" />
          <Image 
            style={styles.rectanglelog}
            source={require('./assets/login1.png')}
          />

          <Text style={styles.baseText}> Bright
            <Text style={styles.innerText}>One</Text>
          </Text>

       
        <View style={styles.textinput}>
          <Text style={styles.txtfirst}>First Name</Text>
          <Image
                style={styles.firsticon}
                source={require('./assets/name.png')}
              />

          <TextInput
              style={styles.inputfirst}
              onChangeText={onChangeText}
              value={text}
            />

          <Text style={styles.txtlast}>Last Name</Text>
          <Image
              style={styles.lasticon}
              source={require('./assets/name.png')}
          />

          <TextInput
              style={styles.inputlast}
              onChangeText={onChangeText}
              value={text}
            />

          <Text style={styles.txtuser}>Email</Text>
          <Image
                style={styles.usericon}
                source={require('./assets/mail.png')}
              />

          <TextInput
              style={styles.inputuser}
              onChangeText={onChangeText}
              value={text}
          />
          <Text style={styles.txtpw}>Password</Text>
          <Image
                style={styles.pwicon}
                source={require('./assets/password.png')}
          />
          <TextInput
            style={styles.inputpw}
            onChangeText={onChangeNumber}
            value={number}
            keyboardType="numeric"
          />
  
          <Text style={styles.txtconfirm}>Confirm Password</Text>
          <Image
                style={styles.confirmicon}
                source={require('./assets/password.png')}
          />
          <TextInput
            style={styles.inputconfirm}
            onChangeText={onChangeNumber}
            value={number}
            keyboardType="numeric"
          />
        </View>


        <TouchableOpacity style={styles.signupBtn}>
          <Text style={styles.signupText}>Sign Up</Text>
        </TouchableOpacity>


        <TouchableOpacity>
          <Text style={styles.btnalready}>Already Have an Account? 
            <Text style={styles.btnlogin}>Sign In</Text>
          </Text>
        </TouchableOpacity>
 
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
      flex:1,
      paddingTop: 30,
      alignItems:'center',
      backgroundColor:'#9B51E0',
    },
    stretch: {
      marginTop: 55,
      width: 87,
      height: 96,
      resizeMode: 'stretch',
    },
    baseText: {
      fontWeight: 'bold',
      fontSize: 20,
      color:'black'
      },
        innerText: {
          color: 'white'
      },
    rectanglelog: {
      position: 'absolute',
      width: 1355,
      height: 585,
      marginTop: 223,
      resizeMode: 'stretch',
    },
    txtfirst: {
      fontWeight: 'bold',
      alignItems:"center",
      justifyContent:"center",
      marginTop: 160,
      fontSize: 11,
      color: '#9B51E0'
    },
    firsticon: {
      position:'absolute',
      top:178,
      right:203,
      width: 18,
      height: 18,
      resizeMode: 'stretch',
    },
    txtlast:{
      fontWeight: 'bold',
      alignItems:"center",
      justifyContent:"center",
      marginTop: 10,
      fontSize: 11,
      color: '#9B51E0'
    },
    lasticon:{
      position:'absolute',
      top:232,
      right:205,
      width: 18,
      height: 18,
      resizeMode: 'stretch',
    },
    txtuser: {
      fontWeight: 'bold',
      alignItems:"center",
      justifyContent:"center",
      marginTop: 10,
      fontSize: 11,
      color: '#9B51E0'
    },
    usericon:{
      position:'absolute',
      top:281,
      right:203,
      width: 22,
      height: 24,
      resizeMode: 'stretch',
    },
    txtpw: {
      fontWeight: 'bold',
      alignItems:"center",
      justifyContent:"center",
      marginTop: 10,
      fontSize: 11,
      color: '#9B51E0'
    },
    pwicon:{
      position:'absolute',
      top:335,
      right:205,
      width: 21,
      height: 21,
      resizeMode: 'stretch',
    },
    txtconfirm: {
      fontWeight: 'bold',
      alignItems:"center",
      justifyContent:"center",
      marginTop: 10,
      fontSize: 11,
      color: '#9B51E0'
    },
    confirmicon: {
      position:'absolute',
      top:388,
      right:205,
      width: 21,
      height: 21,
      resizeMode: 'stretch',
    },
    inputfirst: {
      height: 30,
      width:200,
      marginTop: 0,
      alignItems:"center",
      justifyContent:"center",
      borderWidth: 1,
      borderColor:'#9B51E0'
    },
    inputlast:{
      height: 30,
      width:200,
      marginTop:0,
      alignItems:"center",
      justifyContent:"center",
      borderWidth: 1,
      borderColor:'#9B51E0'
    },
    inputuser: {
      height: 30,
      width:200,
      marginTop: 0,
      alignItems:"center",
      justifyContent:"center",
      borderWidth: 1,
      borderColor:'#9B51E0'
    },
    inputpw:{
      height: 30,
      width:200,
      marginTop:0,
      alignItems:"center",
      justifyContent:"center",
      borderWidth: 1,
      borderColor:'#9B51E0'
    },
    inputconfirm:{
      height: 30,
      width:200,
      marginTop:0,
      alignItems:"center",
      justifyContent:"center",
      borderWidth: 1,
      borderColor:'#9B51E0'
    },
    signupBtn: {
      width:200,
      borderRadius:25,
      height:30,
      alignItems:"center",
      justifyContent:"center",
      marginTop:10,
      backgroundColor:"#000000",
      top:10
    },
    signupText: {
      color: '#FFFFFF',
      fontWeight: 'bold'
    },
    btnalready: {
      marginTop: 20,
      color: '#9B51E0',
      height: 30,
      paddingTop:10
    },
    btnlogin: {
      marginTop: 20,
      textDecorationLine:'underline',
      color: '#9B51E0',
      height: 30,
    },
});