import { StatusBar } from 'expo-status-bar';
import React from 'react';
import {View, Image, StyleSheet, Text } from 'react-native';

export default function Portfolio() {
    return (
        <View style={styles.container}>
            <View style={{alignItems:'center',width:393, height:97,backgroundColor:'#9B51E0'}}>
                <Image
                    style={styles.profil}
                    source={require('./assets/1617358437201.jpg')}
                />
                <Text style={styles.name}>Salsabilla Choerunnisa Nurzanah</Text>
                <Text style={styles.username}>@salsabillacn</Text>
            </View>
            
            <View style={{marginTop:20,alignItems:'center',width:311, height:169,borderRadius:8, backgroundColor:'#F2F2F2'}}>
                <Text style={styles.pemrog}>Programming Language</Text>
                <Text style={styles.txtc}>Intermediate C++
                <Text style={styles.txtc}>                         50%</Text>
                </Text>
                <Image
                    style={styles.cicon}
                    source={require('./assets/c.png')}
                />
                <Text style={styles.txtc}>Basic Java
                <Text style={styles.txtc}>                                    30%</Text>
                </Text>
                <Image
                    style={styles.cicon}
                    source={require('./assets/java.png')}
                />
                <Text style={styles.txtc}>Basic JavaScript
                <Text style={styles.txtc}>                          20%</Text>
                </Text>
                <Image
                    style={styles.cicon}
                    source={require('./assets/js.png')}
                />
                <Text style={styles.txtc}>Intermediate HTML
                <Text style={styles.txtc}>                      50%</Text>
                </Text>
                <Image
                    style={styles.cicon}
                    source={require('./assets/html.png')}
                />
            </View>

            <View style={{marginTop:20,alignItems:'center',width:311, height:169,borderRadius:8, backgroundColor:'#9B51E0'}}>
                <Text style={styles.pemrog}>Framework</Text>
                <Image
                    style={styles.codeicon}
                    source={require('./assets/ci.png')}
                />
                <Image
                    style={styles.reacticon}
                    source={require('./assets/react.png')}
                />
                <Text style={styles.txtc}>Basic CodeIgniter
                <Text style={styles.txtc}>                     Basic React Native</Text>
                </Text>
    
                <Text style={styles.percentframe}>40%
                <Text style={styles.percentframe}>                                               20% </Text>
                </Text>

                <View style={{marginTop:60,alignItems:'center',width:311, height:169,borderRadius:8, backgroundColor:'#F2F2F2'}}>
                    <Text style={styles.pemrog}>Technology</Text>
                    <Text style={styles.txtc}>Basic Git
                    <Text style={styles.txtc}>                                         20%</Text>
                    </Text>
                    <Image
                        style={styles.cicon}
                        source={require('./assets/git.png')}
                    />
                    <Text style={styles.txtc}>Basic Gitlab
                    <Text style={styles.txtc}>                                    20%</Text>
                    </Text>
                    <Image
                        style={styles.cicon}
                        source={require('./assets/gitlab.png')}
                    />
                    <Text style={styles.txtc}>Basic Sublime
                    <Text style={styles.txtc}>                                40%</Text>
                    </Text>
                    <Image
                        style={styles.cicon}
                        source={require('./assets/sublime.png')}
                    />
                    <Text style={styles.txtc}>Basic Netbeans
                    <Text style={styles.txtc}>                             40%</Text>
                    </Text>
                    <Image
                        style={styles.cicon}
                        source={require('./assets/netbeans.png')}
                    />
                </View>
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex:1,
        paddingTop:0,
        alignItems:'center',
        backgroundColor:'#FFFFFF',
    },
    profil:{
        alignItems:'center', justifyContent:'center', width:47, height: 49, borderRadius:180, marginTop:10
    },
    name:{
        textAlign:'center', fontWeight:'bold', fontSize:16, color:'black'
    },
    username:{
        textAlign:'center', fontSize:14, color:'black'
    },
    pemrog:{
        textAlign:'left', fontSize:20, color:'black',fontWeight:'bold'
    },
    cicon:{
        marginTop:-20,left:-130, width:25, height:23
    },
    txtc:{
        marginTop:15,
    },
    codeicon:{
        marginTop: 20,marginLeft:-155, width:32, height:32
    },
    reacticon:{
        marginLeft:155, width:32, height:32, marginTop:-30
    },
    percentframe:{
        marginTop:5
    },
    });