import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { TouchableOpacity , View, Image, StyleSheet, Text, TextInput } from 'react-native';

export default function Login() {
    const [text, onChangeText] = React.useState(null);
    const [number, onChangeNumber] = React.useState(null);
    return (
        <View style={styles.container}>
          <Image
            style={styles.stretch}
            source={require('./assets/filetype-08-512.png')}
          />
          <StatusBar style="auto" />
          <Image 
            style={styles.rectanglelog}
            source={require('./assets/login1.png')}
            />

          <Text style={styles.baseText}> Bright
            <Text style={styles.innerText}>One</Text>
          </Text>
    
       
          <View style={styles.textinput}>
            <Text style={styles.txtuser}>Username/Email</Text>
            <Image
                style={styles.usericon}
                source={require('./assets/mail.png')}
            />

            <TextInput
                style={styles.inputuser}
                onChangeText={onChangeText}
                value={text}
              />
            <Text style={styles.txtpw}>Password</Text>
            <Image
                  style={styles.pwicon}
                  source={require('./assets/password.png')}
                />
            <TextInput
              style={styles.inputpw}
              onChangeText={onChangeNumber}
              value={number}
              keyboardType="numeric"
            />
  
          </View>
      <TouchableOpacity style={styles.loginBtn}>
        <Text style={styles.loginText}>Sign In</Text>
      </TouchableOpacity>


      <TouchableOpacity>
        <Text style={styles.btndont}>Don't Have an Account? 
          <Text style={styles.btnsign}>Create Here</Text>
        </Text>
      </TouchableOpacity>
 
    </View>
  );
}
  
  const styles = StyleSheet.create({
    container: {
      flex:1,
      paddingTop: 30,
      alignItems:'center',
      backgroundColor:'#9B51E0',
    },
    stretch: {
      marginTop: 55,
      width: 87,
      height: 96,
      resizeMode: 'stretch',
    },
    baseText: {
      fontWeight: 'bold',
      fontSize: 20,
      color:'black'
      },
    innerText: {
      color: 'white'
    },
    rectanglelog: {
      position: 'absolute',
      width: 1355,
      height: 585,
      marginTop: 223,
      resizeMode: 'stretch',
    },
    txtuser: {
      fontWeight: 'bold',
      alignItems:"center",
      justifyContent:"center",
      marginTop: 170,
      fontSize: 11,
      color: '#9B51E0'
    },
      usericon:{
      position:'absolute',
      top:185,
      right:203,
      width: 25,
      height: 25,
      resizeMode: 'stretch',
    },
    txtpw: {
      fontWeight: 'bold',
      alignItems:"center",
      justifyContent:"center",
      marginTop: 10,
      fontSize: 11,
      color: '#9B51E0'
    },
    pwicon:{
      position:'absolute',
      top:240,
      right:205,
      width: 21,
      height: 21,
      resizeMode: 'stretch',
    },
    inputuser: {
      height: 30,
      width:200,
      marginTop: 0,
      alignItems:"center",
      justifyContent:"center",
      borderWidth: 1,
      borderColor:'#9B51E0'
    },
    inputpw:{
      height: 30,
      width:200,
      marginTop:0,
      alignItems:"center",
      justifyContent:"center",
      borderWidth: 1,
      borderColor:'#9B51E0'
    },
    loginBtn:{
      width:200,
      borderRadius:25,
      height:30,
      alignItems:"center",
      justifyContent:"center",
      marginTop:30,
      backgroundColor:"#000000",
    },
    loginText: {
      color: '#FFFFFF',
      fontWeight: 'bold'
    },
    btndont: {
      marginTop: 100,
      color: '#9B51E0',
      height: 30,
    },
    btnsign: {
      marginTop: 100,
      textDecorationLine:'underline',
      color: '#9B51E0',
      height: 30,
    },
  });
  
