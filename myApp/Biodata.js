import React from 'react';
import {View, Image, StyleSheet, Text } from 'react-native';

export default function Biodata() {
  return (
      <View style={styles.container}>
          <View style={{alignItems:'center',width:393, height:230,backgroundColor:'#9B51E0'}}>
            <Image
              style={styles.profil}
              source={require('./assets/1617358437201.jpg')}
            />
            <Text style={styles.name}>Salsabilla Choerunnisa Nurzanah</Text>
            <Text style={styles.username}>@salsabillacn</Text>
            <Text style={styles.username}>Purwakarta, Jawa Barat</Text>
            <Text style={styles.username}>STT Wastukancana</Text>
          </View>
          <View style={{alignItems:'center',width:393, height:25,backgroundColor:'#F2F2F2'}}>
            <Text style={styles.info}>Teknik Informatika</Text>
            <Image
              style={styles.schoolicon}
              source={require('./assets/school.png')}
            />
          </View>
          <View style={{ alignItems:'center',width:393, height:25,backgroundColor:'#9999CC'}}>
              <Text style={styles.info}>Pagi A</Text>
              <Image
                style={styles.bookicon}
                source={require('./assets/book.png')}
              />   
          </View>
          <View style={styles.biodata}>
            <Text style={styles.contact}>CONTACT</Text>  
            <Text style={styles.email}>Line</Text>
            <Text style={styles.sosmed}>salsabillaaacn</Text>
            <Text style={styles.email}>Email</Text>
            <Text style={styles.sosmed}>salsabillachoerunnisa08@gmail.com</Text> 
            <Text style={styles.email}>Twitter</Text>
            <Text style={styles.sosmed}>@salsabilla_dr</Text>
            <Text style={styles.email}>Instagram</Text>
            <Text style={styles.sosmed}>salsabillacn</Text>
            <Text style={styles.email}>Facebook</Text>
            <Text style={styles.sosmed}>Salsabilla Asyhari</Text>
            <Text style={styles.email}>TikTok</Text>
            <Text style={styles.sosmed}>@JustForFunSaale</Text>
            <Text style={styles.email}>Telegram</Text>
            <Text style={styles.sosmed}>@salebcn</Text>
        </View>
      </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex:1,
    paddingTop:0,
    alignItems:'center',
    backgroundColor:'#FFFFFF',
  },
  profil:{
    top:20,
    alignItems:'center',
    justifyContent:'center',
    width:100,
    height: 102, 
    borderRadius:180, 
    marginTop:10
  },
  name:{
    marginTop: 30,
    textAlign:'center',
    fontWeight:'bold',
    fontSize:18, 
    color:'black'
  },
  username:{
    textAlign:'center', 
    fontSize:14, 
    color:'black'
  },
  info:{
    textAlign:'center', 
    fontWeight:'bold', 
    fontSize:18, 
    color:'black'
  },
  schoolicon:{
    width:17, 
    height:20,
    marginLeft:-155,
    marginTop:-19
  },
  bookicon:{
    width:15,
    height:15, 
    marginLeft:-70, 
    marginTop:-16
  },
  contact:{
    paddingTop:10,
    paddingBottom:0,
    paddingLeft:40,
    fontSize:18,
    color:'black',
    fontWeight:'bold',
    textDecorationLine:'underline',
  },
  email:{
     fontSize:14,
     color:'#9B51E0',
     fontWeight:'bold',
     paddingTop:10,
     paddingLeft:80
  },
  biodata:{
    backgroundColor:'white',
    width: 393,
    height:580,
  },
  sosmed:{
    fontSize:12,
    color:'black',
    paddingLeft:100,
    marginBottom:10,
  },
});