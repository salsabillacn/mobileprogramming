//A. Looping While
var bilangan=1;
console.log("Output Looping While")
console.log("LOOPING PERTAMA");

while (bilangan <=20) {
    if (bilangan%2 == 0){
        console.log(bilangan + " - I love coding");
    }
    bilangan++;
}

var bilangan=20;
console.log("\nLOOPING KEDUA");

while(bilangan>=1){
    if(bilangan%2==0){
        console.log(bilangan + " - I will become a mobile developer");
    }
    bilangan--;
} 

//B. Looping menggunakan For
var bilangan;

console.log("\nOutput Looping For")
for (bilangan=1;bilangan<=20;bilangan++){
    if (bilangan%2==0){
        console.log(bilangan + " - Informatika");
    }else 
    if (bilangan%3==0){
        console.log(bilangan + " - I Love Coding");
    }else
        console.log(bilangan + " - Teknik");      
}

//C. Membuat Persegi Panjang #
var sisi="";

console.log("\nOutput Persegi Panjang #")

for(var bil1=0; bil1<4; bil1++){
    for(var bil2=0; bil2<8; bil2++){
        sisi += "#";
    }
    sisi += "\n";
}
console.log(sisi);

//D. Membuat Tangga
var sisi="";

console.log("\nOutput Membuat Tangga")

for(var bil1=0; bil1<=7; bil1++){
    for(var bil2=0; bil2<bil1; bil2++){
        sisi += "#";
    }
    sisi += "\n";
}
console.log(sisi);

//E. Membuat Papan Catur
var sisi="";
var panjang=8;
var lebar=8;

console.log("\nOutput Membuat Papan Catur")

for(var bil1=1; bil1<=lebar; bil1++){
    if(bil1%2==0){
        for(var bil2=1; bil2<=panjang; bil2++){
            if(bil2%2==0){
                sisi += " ";
            }else {
                sisi += "#";
            }
            
        }
    } else {
        for(var bil2=1; bil2<=panjang; bil2++){
            if(bil2%2==0){
                sisi += "#";
            }else{
                sisi += " ";
            }
        }
    }

    sisi += "\n";
}
console.log(sisi);