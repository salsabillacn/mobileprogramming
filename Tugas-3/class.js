//No. 1 - Animal Class
console.log("No. 1 - Animal Class")
//Release 0
class Animal {
    constructor(name){
        this._name=name
        this._legs=4
        this._cold_blooded=false
    }
    get name(){
        return this._name
    }
    set name(a){
        this._name=a
    }
    get legs(){
        return this._legs
    }
    set legs(b){
        this._legs=b
    }
    get cold_blooded(){
        return this._cold_blooded
    }
    set cold_blooded(c){
        this._cold_blooded=c
    }
}
    let sheep = new Animal("shaun");
    console.log(sheep.name) // "shaun"
    console.log(sheep.legs) // 4
    console.log(sheep.cold_blooded) // false

//Release 1
class Ape extends Animal{
    constructor(name){
        super(name);
        this._name=name;
        this._legs=2;
    }
    yell(){
        console.log("Auoo")
        return
    }
}

class Frog extends Animal{
    constructor(name){
        super(name);
        this._name=name;
    }
    jump(){
        console.log("hop hop") 
        return
    }
}
let sungokong = new Ape("kera sakti")
sungokong.yell() // "Auooo"

let kodok = new Frog("buduk")
kodok.jump() // "hop hop"

//No.2 - Function to Class
console.log("\nNo.2 - Function to Class")
class Clock {
    constructor({template}) {
        this.template = template;
    }
    render() {
        let date = new Date();
  
        let hours = date.getHours();
        if (hours < 10) hours = '0' + hours;
        
        let mins = date.getMinutes();
        if (mins < 10) mins = '0' + mins;
        
        let secs = date.getSeconds();
        if (secs < 10) secs = '0' + secs;
        
        let output = this.template
        .replace('h', hours)
        .replace('m', mins)
        .replace('s', secs);
        
        console.log(output);
    }
    stop() {
        clearInterval(this.timer);
    }
    start() {
        this.render();
        this.timer = setInterval(() => this.render(), 1000);
    }
}
let clock = new Clock({template: 'h:m:s'});
clock.start();