//No. 1 (Range)
function range(startNum, finishNum){

    var numbers=[];

    if(startNum<finishNum){
        for (var i=startNum; i<=finishNum; i++){
            numbers.push(i); 
        } 
    } else if(startNum>finishNum){
        for (var j=startNum; j>=finishNum; j--){
            numbers.push(j); 
        }
    } else if(startNum==null && finishNum==null){
       numbers.push(-1);
    } else if(startNum==null){
        numbers.push(-1);
    } else if(finishNum==null){
        numbers.push(-1);
   }
    return numbers;
}

console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11,18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range())

//No.2 (Range with Step)
function rangeWithStep(startNum, finishNum, step){

    var numbers=[];

    if(startNum<finishNum){
        for (var i=startNum; i<=finishNum; i += step){
            numbers.push(i); 
        } 
    } else if(startNum>finishNum){
        for (var j=startNum; j>=finishNum; j -= step){
            numbers.push(j); 
        }
    }
    return numbers;
 }
console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5]

//No. 3 (Sum of Range)
function sum(startNum, finishNum, step) {
    var  number = []
    
    if (startNum == null && finishNum == null && step == null) {
        number.push(0);
    }
    else if (startNum < finishNum) {
        if (step == null){
            step = 1
        }
        for (let a = startNum; a<=finishNum; a+=step) {
            number.push(a);
        }
    }
    else if (startNum > finishNum) {
        if (step == null) {
            step = 1
        }
        for (let a = startNum; a>=finishNum; a-=step) {
            number.push(a);
        }
    }
    else if (startNum == null || finishNum == null) {
        number.push(startNum);
    }
    let total = number.reduce((val, nilai) => {
        return val + nilai;
    }, 0)
    return total;
}
console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0


//No. 4 (Array Multidimensi)
var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
]

function dataHandling(){
    for (var i=0; i<input.length; i++){
        console.log('Nomor ID: ' + input[i][0])
        console.log('Nama Lengkap: ' + input[i][1])
        console.log('TTL: ' + input[i][2] + ' ' + input[i][3])
        console.log('Hobi: ' + input[i][4] + '\n')
    }
}
dataHandling()

//No.5 (Balik Kata)
function balikKata(string) {
    
    var str = ""
    
    for (var i=string.length-1; i>=0; i--) {
        str += string[i]
    }
    return str
}
console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("Informatika")) // akitamrofnI
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Humanikers")) // srekinamuH ma I

//No.6 (Metode Array)
var input = ["0001", "Roman Alamsyah", "Bandar Lampung", 
            "21/05/1989", "Memvbaca"];

function dataHandling2(data){
    input.splice(1,1, 'Roman Alamsyah Elsharawy');
    input.splice(2, 1, 'Provinsi Bandar Lampung');
    input.splice(4,1);
    input.splice(4, 2, 'Pria', 'SMA Internasional Metro');
    console.log(input)

    var inpsplit = input[3].split("/")
    var inpjoin = inpsplit.join("-")
    var bulan = inpjoin[3] + inpjoin[4]

    switch(bulan){
        case '01': console.log('Januari');
            break;
        case '02': console.log('Februari');
            break;
        case '03': console.log('Maret');
            break;
        case '04': console.log('April');
            break;
        case '05': console.log('Mei');
            break;
        case '06': console.log('Juni');
            break;
        case '07': console.log('Juli');
            break;
        case '08': console.log('Agustus');
            break;
        case '09': console.log('September');
            break;
        case '10': console.log('Oktober');
            break;
        case '11': console.log('November');
            break;
        case '12': console.log('Desember');
            break;
    }
     
    var inpshortret= inpsplit.sort(function (a,b){ return b - a});
    console.log(inpshortret)

    var tes = input[3].split("/")
    console.log(tes.join("-"))

    console.log(input[1].toString().slice(0, 14))

    return data
}
dataHandling2(input);