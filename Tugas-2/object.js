//No.1 (Array to Object)
function arrayToObject(arr) {   
    for(var i = 0; i < arr.length; i++){
      var data = {}
      console.log(i+1+'. '+arr[i][0]+' '+arr[i][1]+':');
      data.firstName = arr[i][0]
      data.lastName = arr[i][1]
      data.gender = arr[i][2];
      if(arr[i][3] === undefined){
          data.age = 'Invalid Birth Year'
      } else if (arr[i][3] > 2020){
          data.age = 'Invalid Birth Year'
      } else {
          data.age = 2020 - arr[i][3];
      }
      console.log(data)
    }
    
    if(arr.length === 0){
      console.log('""')
    }
}

var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha",
"Romanoff", "female"] ] 
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots",
"female", 2023] ]
arrayToObject(people);
arrayToObject(people2);

//No. 2 (Shopping Time)
function shoppingTime(memberId, money) {
    var belanja = {}
    var barang = [
                    ['Sepatu Stacattu', 1500000],
                    ['Baju Zoro', 500000],
                    ['Baju H&N', 250000],
                    ['Sweater Uniklooh', 175000],
                    ['Casing Handphone', 50000]
                 ]
    
    if(memberId === undefined && money === undefined){
        return "Mohon maaf, toko X hanya berlaku untuk member saja"
    }
    
    if(memberId === ''){
        return "Mohon maaf, toko X hanya berlaku untuk member saja"
    } else {
            belanja.memberId = memberId
        }
    
    if(money <= 50000 ){
        return "Mohon maaf, uang tidak cukup"
    } else {
            belanja.money = money
        }
    
    var jumlah = 0
    belanja.listPurchased = []
    for(let i = 0; i < barang.length; i++){
      
      if(money > barang[i][1]){
        belanja.listPurchased.push(barang[i][0])
        jumlah += barang[i][1]
      }
      belanja.changeMoney = money - jumlah
    }
     return belanja
  }
  
console.log(shoppingTime('1820RzKrnWn08', 2475000));
console.log(shoppingTime('82Ku8Ma742', 170000));
console.log(shoppingTime('', 2475000)); 
console.log(shoppingTime('234JdhweRxa53', 15000)); 
console.log(shoppingTime());

//No. 3 (Naik Angkot)
function naikAngkot(listPenumpang) {
    rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    
    var angkot = [{},{}];
    var i=0;
    var asal = '';
    var tujuan = '';
  
    for (i; i<listPenumpang.length; i++) {
        var j = 0;
  
        for (j; j<listPenumpang[i].length; j++) {
            
            switch (j) {
                case 0: {
                    angkot[i].penumpang = listPenumpang[i][j];
                    break;
                } case 1: {
                    angkot[i].naikDari = listPenumpang[i][j];
                    angkot[i].tujuan = listPenumpang[i][j+1];
                    break;
                } case 2: {
                    asal = listPenumpang[i][j-1];
                    tujuan = listPenumpang[i][j];
                    var jarak = 0;
                    for (var k=0; k<rute.length; k++) {
                        if (rute[k] === asal) {
                            for (var l=k+1; l<rute.length; l++) {
                                jarak += 1;
                                if (rute[l] === tujuan) {
                                    var bayar = jarak * 2000;
                                    angkot[i].bayar = bayar;
                                }
                            }
                        }
                    }
                    break;
                }
            }
        }
    }

return angkot;
}

console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));